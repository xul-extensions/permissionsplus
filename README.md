Enhances the permission manager on UXP browsers with additional site specific permissions. 

**Downloads**

- [For Pale Moon](https://addons.palemoon.org/addon/pmplus/)