if ("undefined" == typeof(PMPlus)) {
	var PMPlus = {};
}
Components.utils.import("chrome://permissionsplus/content/common.jsm",PMPlus);
PMPlus.PageInfo={
		txtUserAgent:null,
		btnSave:null,
		btnReset:null,
		docURI : null,
		host:null,
		prefListenerUA:null,
		prefObserver:null,
		sectionClipboard:null,
		rbClipboardAllow:null,
		rbClipboardBlock:null,
		rbgClipboard:null,
		sectionctxMenu:null,
		rbctxMenuAllow:null,
		rbctxMenuBlock:null,
		rbgctxMenu:null,
		sectionHistory:null,
		rbHistoryAllow:null,
		rbHistoryBlock:null,
		rbgHistory:null,
		init:function(){
			PMPlus.PageInfo.txtUserAgent=document.getElementById("pplus-pinfo-txtuseragent");
			PMPlus.PageInfo.btnSave=document.getElementById("pplus-pinfo-btnsave");
			PMPlus.PageInfo.btnReset=document.getElementById("pplus-pinfo-btnreset");
			PMPlus.PageInfo.sectionClipboard=document.getElementById("permClipboardRow");
			PMPlus.PageInfo.sectionctxMenu=document.getElementById("permctxMenuRow");
			PMPlus.PageInfo.sectionHistory=document.getElementById("permHistoryRow");
			PMPlus.Common.globals.chkPgClipboard=document.getElementById("pplus-pinfo-chkclipboard");
			PMPlus.Common.globals.chkPgctxMenu=document.getElementById("pplus-pinfo-chkctxmenu");
			PMPlus.Common.globals.chkPgHistory=document.getElementById("pplus-pinfo-chkhistory");
			PMPlus.PageInfo.rbgClipboard=document.getElementById("pplus-pinfo-rbgclipboard");
			PMPlus.PageInfo.rbClipboardAllow=document.getElementById("pplus-pinfo-rbclipboard-allow");
			PMPlus.PageInfo.rbClipboardBlock=document.getElementById("pplus-pinfo-rbclipboard-block");
			PMPlus.PageInfo.rbgctxMenu=document.getElementById("pplus-pinfo-rbgctxmenu");
			PMPlus.PageInfo.rbctxMenuAllow=document.getElementById("pplus-pinfo-rbctxmenu-allow");
			PMPlus.PageInfo.rbctxMenuBlock=document.getElementById("pplus-pinfo-rbctxmenu-block");
			PMPlus.PageInfo.rbgHistory=document.getElementById("pplus-pinfo-rbghistory");
			PMPlus.PageInfo.rbHistoryAllow=document.getElementById("pplus-pinfo-rbhistory-allow");
			PMPlus.PageInfo.rbHistoryBlock=document.getElementById("pplus-pinfo-rbhistory-block");
			PMPlus.PageInfo.btnReset.addEventListener("command",PMPlus.PageInfo.onBtnReset);
			PMPlus.PageInfo.btnSave.addEventListener("command",PMPlus.PageInfo.onBtnSave);
			PMPlus.PageInfo.txtUserAgent.addEventListener("input",PMPlus.PageInfo.onUAChange);
			PMPlus.Common.globals.chkPgClipboard.addEventListener("command",PMPlus.PageInfo.onchkDefault);
			PMPlus.Common.globals.chkPgClipboard.setAttribute("permission","pmplus-clipboard");
			PMPlus.Common.globals.chkPgClipboard.setAttribute("global-permission","dom.event.clipboardevents.enabled");
			PMPlus.Common.globals.chkPgctxMenu.addEventListener("command",PMPlus.PageInfo.onchkDefault);
			PMPlus.Common.globals.chkPgctxMenu.setAttribute("permission","pmplus-contextmenu");
			PMPlus.Common.globals.chkPgctxMenu.setAttribute("global-permission","dom.event.contextmenu.enabled");
			PMPlus.Common.globals.chkPgHistory.addEventListener("command",PMPlus.PageInfo.onchkDefault);
			PMPlus.Common.globals.chkPgHistory.setAttribute("permission","pmplus-history");
			PMPlus.Common.globals.chkPgHistory.setAttribute("global-permission","places.history.enabled");
			PMPlus.PageInfo.rbgClipboard.addEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.PageInfo.rbgClipboard.setAttribute("permission","pmplus-clipboard");
			PMPlus.PageInfo.rbgctxMenu.addEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.PageInfo.rbgctxMenu.setAttribute("permission","pmplus-contextmenu");
			PMPlus.PageInfo.rbgHistory.addEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.PageInfo.rbgHistory.setAttribute("permission","pmplus-history");
			PMPlus.PageInfo.btnSave.setAttribute("disabled","true");
			PMPlus.PageInfo.docURI = window.opener.gBrowser.selectedBrowser.currentURI;
			if(PMPlus.PageInfo.docURI.scheme.startsWith('http')){
				PMPlus.PageInfo.host = PMPlus.PageInfo.docURI.host;
			} else {
				PMPlus.Common.printlog("Not on a webpage - "+gDocument.documentURI);
				return;
			}
			if(PMPlus.PageInfo.host.startsWith('www.')){
				PMPlus.PageInfo.host=PMPlus.PageInfo.host.replace('www.','');
			}
			PMPlus.PageInfo.rbgClipboard.setAttribute("pageurl",PMPlus.PageInfo.docURI.spec);
			PMPlus.PageInfo.rbgctxMenu.setAttribute("pageurl",PMPlus.PageInfo.docURI.spec);
			PMPlus.PageInfo.rbgHistory.setAttribute("pageurl",PMPlus.PageInfo.docURI.spec);
			PMPlus.PageInfo.prefListenerUA.register();
			PMPlus.PageInfo.prefListenerDOM.register();
			Services.obs.addObserver(PMPlus.PageInfo.permissionsObserver,"perm-changed",false);
			PMPlus.PageInfo.txtUserAgent.value=PMPlus.Common.getOverride(PMPlus.PageInfo.host);
			if(PMPlus.PageInfo.txtUserAgent.value=="" || !PMPlus.Common.isOverridden(PMPlus.PageInfo.host)){
				PMPlus.PageInfo.txtUserAgent.placeholder=PMPlus.Common.getPlaceHolderText(PMPlus.PageInfo.host);
			}
			if(!Services.prefs.getBoolPref("dom.event.clipboardevents.enabled",true)) {
				PMPlus.PageInfo.sectionClipboard.setAttribute("hidden","true");
			}
			if(!Services.prefs.getBoolPref("dom.event.contextmenu.enabled",true)) {
				PMPlus.PageInfo.sectionctxMenu.setAttribute("hidden","true");
			}
			if(!Services.prefs.getBoolPref("places.history.enabled",true)) {
				PMPlus.PageInfo.sectionHistory.setAttribute("hidden","true");
			}
			let permclipboard = Services.perms.testPermission(PMPlus.PageInfo.docURI,"pmplus-clipboard");
			let permctxmenu = Services.perms.testPermission(PMPlus.PageInfo.docURI,"pmplus-contextmenu");
			let permhistory = Services.perms.testPermission(PMPlus.PageInfo.docURI,"pmplus-history");
			PMPlus.Common.printlog("Clipboard permission for page "+PMPlus.PageInfo.docURI.spec+" is "+permclipboard);
			PMPlus.Common.printlog("Context-menu permission for page "+PMPlus.PageInfo.docURI.spec+" is "+permctxmenu);
			PMPlus.Common.printlog("History permission for page "+PMPlus.PageInfo.docURI.spec+" is "+permhistory);
			PMPlus.Common.initPermissions(permclipboard,PMPlus.PageInfo.rbgClipboard,
			PMPlus.Common.globals.chkPgClipboard,"dom.event.clipboardevents.enabled");
			PMPlus.Common.initPermissions(permctxmenu,PMPlus.PageInfo.rbgctxMenu,
			PMPlus.Common.globals.chkPgctxMenu,"dom.event.clipboardevents.enabled");
			PMPlus.Common.initPermissions(permhistory,PMPlus.PageInfo.rbgHistory,
			PMPlus.Common.globals.chkPgHistory,"places.history.enabled");
		},
		/**
		 * Event handler for save button.
		 */
		onBtnSave:function(event){
			PMPlus.Common.prefBranchUA.setCharPref(PMPlus.PageInfo.host,PMPlus.PageInfo.txtUserAgent.value);
			event.target.setAttribute("disabled","true");
		},
		/**
		 * Event handler for reset button.
		 */
		onBtnReset:function(){
			PMPlus.Common.prefBranchUA.clearUserPref(PMPlus.PageInfo.host);
			PMPlus.PageInfo.txtUserAgent.placeholder=PMPlus.Common.prefBranchUA.getCharPref(PMPlus.PageInfo.host,"");
			PMPlus.PageInfo.txtUserAgent.value="";
			PMPlus.PageInfo.txtUserAgent.placeholder=PMPlus.Common.getPlaceHolderText(PMPlus.PageInfo.host);
			PMPlus.PageInfo.btnSave.setAttribute("disabled","true");
		},
		/**
		 * Event handler for UA textbox change.
		 */
		onUAChange:function(event){
			if(event.target.value.length==0||
				event.target.value==PMPlus.Common.getOverride(PMPlus.PageInfo.host)){
				PMPlus.PageInfo.btnSave.setAttribute("disabled","true");
			} else {
				PMPlus.PageInfo.btnSave.removeAttribute("disabled");
			}
		},
		/**
		 * Eventhandler for default permission checkbox. 
		 */
		onchkDefault : function(event){
			switch(event.target){
				case PMPlus.Common.globals.chkPgClipboard:
					PMPlus.Common.useDefaultHandler(PMPlus.PageInfo.docURI,
					event.target,PMPlus.PageInfo.rbgClipboard,
					PMPlus.Common.globals.chkPmgrClipboard);
					break;
				case PMPlus.Common.globals.chkPgctxMenu:
					PMPlus.Common.useDefaultHandler(PMPlus.PageInfo.docURI,
					event.target,PMPlus.PageInfo.rbgctxMenu,
					PMPlus.Common.globals.chkPmgrctxMenu);
					break;	
				case PMPlus.Common.globals.chkPgHistory:
					PMPlus.Common.useDefaultHandler(PMPlus.PageInfo.docURI,
					event.target,PMPlus.PageInfo.rbgHistory,
					PMPlus.Common.globals.chkPmgrHistory);
					break;	
				default:break;	
			}
		},
		/**
		 * Pref listener for user agent changes.
		 */
		prefListenerUA:new PMPlus.PMPlusPrefListener("general.useragent.override.",function(branch,name){
			if(name==PMPlus.PageInfo.host){
				PMPlus.PageInfo.txtUserAgent.value=PMPlus.Common.getOverride(PMPlus.PageInfo.host);
			}
		}),
		/**
		 * Observes permission changes.
		 */
		permissionsObserver : {
			observe : function(aSubject,aTopic,aData){
				if(aTopic == "perm-changed") {
					if(aData == "deleted"){
						if(aSubject.type == "pmplus-clipboard"){
							PMPlus.Common.globals.chkPgClipboard.checked = true;
							PMPlus.PageInfo.rbgClipboard.disabled = true;
							PMPlus.PageInfo.rbgClipboard.selectedIndex = Services.prefs.getBoolPref("dom.event.clipboardevents.enabled",true) ? 0 : 1;
						} else if(aSubject.type == "pmplus-contextmenu"){
							PMPlus.Common.globals.chkPgctxMenu.checked = true;
							PMPlus.PageInfo.rbgctxMenu.disabled = true;
							PMPlus.PageInfo.rbgctxMenu.selectedIndex = Services.prefs.getBoolPref("dom.event.contextmenu.enabled",true) ? 0 : 1;
						} else if(aSubject.type == "pmplus-history"){
							PMPlus.Common.globals.chkPgHistory.checked = true;
							PMPlus.PageInfo.rbgHistory.disabled = true;
							PMPlus.PageInfo.rbgHistory.selectedIndex = Services.prefs.getBoolPref("places.history.enabled",true) ? 0 : 1;
						}
					} else if(aData == "added" || aData == "changed"){
						if(aSubject.type == "pmplus-clipboard"){
							PMPlus.Common.globals.chkPgClipboard.checked = false;
							PMPlus.PageInfo.rbgClipboard.selectedIndex = aSubject.capability - 1;
							PMPlus.PageInfo.rbgClipboard.disabled = false;
						} else if(aSubject.type == "pmplus-contextmenu"){
							PMPlus.Common.globals.chkPgctxMenu.checked = false;
							PMPlus.PageInfo.rbgctxMenu.selectedIndex = aSubject.capability - 1;
							PMPlus.PageInfo.rbgctxMenu.disabled = false;
						} else if(aSubject.type == "pmplus-history"){
							PMPlus.Common.globals.chkPgHistory.checked = false;
							PMPlus.PageInfo.rbgHistory.selectedIndex = aSubject.capability - 1;
							PMPlus.PageInfo.rbgHistory.disabled = false;
						}
					} 
				}
			}	
		},
		/**
		 * Preference listener for global DOM preference.
		 */
		prefListenerDOM:new PMPlus.PMPlusPrefListener("dom.event.",function(branch,name){
			var hidden = false;
			switch(name){
				case("clipboardevents.enabled"):
					hidden = !branch.getBoolPref("clipboardevents.enabled",true);
					PMPlus.Common.hideSection(hidden,
					PMPlus.PageInfo.rbgClipboard,PMPlus.PageInfo.sectionClipboard);
					break;
				case("contextmenu.enabled"):
					hidden = !branch.getBoolPref("clipboardevents.enabled",true);
					PMPlus.Common.hideSection(hidden,
					PMPlus.PageInfo.rbgctxMenu,PMPlus.PageInfo.sectionctxMenu);
					break;	
				default:
					break;
			}
		}),		
			cleanup:function(){
			Services.obs.removeObserver(PMPlus.PageInfo.permissionsObserver,"perm-changed");
			PMPlus.PageInfo.rbgHistory.removeEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.PageInfo.rbgctxMenu.removeEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.PageInfo.rbgClipboard.removeEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.Common.globals.chkPgctxMenu.removeEventListener("command",PMPlus.PageInfo.onchkDefault);
			PMPlus.Common.globals.chkPgClipboard.removeEventListener("command",PMPlus.PageInfo.onchkDefault);
			PMPlus.Common.globals.chkPgHistory.removeEventListener("command",PMPlus.PageInfo.onchkDefault);
			PMPlus.PageInfo.btnSave.removeEventListener("command",PMPlus.PageInfo.onBtnSave);
			PMPlus.PageInfo.btnReset.removeEventListener("command",PMPlus.PageInfo.onBtnReset);
			PMPlus.PageInfo.txtUserAgent.removeEventListener("input",PMPlus.PageInfo.onUAChange);
			PMPlus.PageInfo.prefListenerDOM.unregister();
			PMPlus.PageInfo.prefListenerUA.unregister();
			PMPlus.Common.globals.chkPgClipboard = null;
			PMPlus.Common.globals.chkPgctxMenu = null;
			PMPlus.Common.globals.chkPgHistory = null;
			window.removeEventListener("load",PMPlus.PageInfo.init);
		}
}
window.addEventListener("load",PMPlus.PageInfo.init);
window.addEventListener("unload",PMPlus.PageInfo.cleanup);