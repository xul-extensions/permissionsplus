if ("undefined" == typeof(PMPlus)) {
	var PMPlus = {};
}
Components.utils.import("chrome://permissionsplus/content/common.jsm",PMPlus);
PMPlus.Pmgr={
		txtUserAgent : null,
		btnSave : null,
		btnReset : null,
		rbClipboardAllow : null,
		rbClipboardBlock : null,
		rbgClipboard : null,
		rbctxMenuAllow : null,
		rbctxMenuBlock : null,
		rbgctxMenu : null,
		rbHistoryAllow : null,
		rbHistoryBlock : null,
		rbgHistory : null,
		sectionUAHeader : null,
		sectionUAControls : null,
		sectionClipboardHeader : null,
		sectionClipboardControls : null,
		sectionCtxMenuHeader : null,
		sectionCtxMenuControls : null,
		sectionHistoryHeader : null,
		sectionHistoryControls : null,
		lstSites : null,
		host : null,
		docURI : null,
		init:function(){
			PMPlus.Pmgr.txtUserAgent = document.getElementById("pplus-pmgr-txtuseragent");
			PMPlus.Pmgr.btnSave = document.getElementById("pplus-pmgr-btnsave");
			PMPlus.Pmgr.btnReset = document.getElementById("pplus-pmgr-btnreset");
			PMPlus.Pmgr.lstSites = document.getElementById("sites-list");
			PMPlus.Pmgr.sectionUAHeader = document.getElementById("permissionplus-pmgr-section-ua-header");
			PMPlus.Pmgr.sectionUAControls = document.getElementById("permissionplus-pmgr-section-ua-controls");
			PMPlus.Pmgr.sectionClipboardHeader = document.getElementById("permissionplus-pmgr-section-clipboard-header");
			PMPlus.Pmgr.sectionClipboardControls = document.getElementById("permissionplus-pmgr-section-clipboard-controls");
			PMPlus.Pmgr.sectionHistoryHeader = document.getElementById("permissionplus-pmgr-section-history-header");
			PMPlus.Pmgr.sectionHistoryControls = document.getElementById("permissionplus-pmgr-section-history-controls");
			PMPlus.Common.globals.chkPmgrClipboard = document.getElementById("pplus-pmgr-chkclipboard");
			PMPlus.Common.globals.chkPmgrctxMenu=document.getElementById("pplus-pmgr-chkctxmenu");
			PMPlus.Common.globals.chkPmgrHistory=document.getElementById("pplus-pmgr-chkhistory");
			PMPlus.Pmgr.rbgClipboard = document.getElementById("pplus-pmgr-rbgclipboard");
			PMPlus.Pmgr.rbClipboardAllow = document.getElementById("pplus-pmgr-rbclipboard-allow");
			PMPlus.Pmgr.rbClipboardBlock = document.getElementById("pplus-pmgr-rbclipboard-block");
			PMPlus.Pmgr.sectionCtxMenuHeader = document.getElementById("permissionplus-pmgr-section-ctxmenu-header");
			PMPlus.Pmgr.sectionCtxMenuControls = document.getElementById("permissionplus-pmgr-section-ctxmenu-controls");
			PMPlus.Pmgr.rbgctxMenu = document.getElementById("pplus-pmgr-rbgctxmenu");
			PMPlus.Pmgr.rbctxMenuAllow = document.getElementById("pplus-pmgr-rbgctxmenu-allow");
			PMPlus.Pmgr.rbctxMenuBlock = document.getElementById("pplus-pmgr-rbgctxmenu-block");
			PMPlus.Pmgr.rbgHistory = document.getElementById("pplus-pmgr-rbghistory");
			PMPlus.Pmgr.rbHistoryAllow = document.getElementById("pplus-pmgr-rbghistory-allow");
			PMPlus.Pmgr.rbHistoryBlock = document.getElementById("pplus-pmgr-rbghistory-block");
			PMPlus.Pmgr.btnReset.addEventListener("command",PMPlus.Pmgr.onBtnReset);
			PMPlus.Pmgr.btnSave.addEventListener("command",PMPlus.Pmgr.onBtnSave);
			PMPlus.Pmgr.txtUserAgent.addEventListener("input",PMPlus.Pmgr.onUAChange);
			PMPlus.Common.globals.chkPmgrClipboard.addEventListener("command",PMPlus.Pmgr.onchkDefault);
			PMPlus.Common.globals.chkPmgrClipboard.setAttribute("permission","pmplus-clipboard");
			PMPlus.Common.globals.chkPmgrClipboard.setAttribute("global-permission","dom.event.clipboardevents.enabled");
			PMPlus.Common.globals.chkPmgrctxMenu.addEventListener("command",PMPlus.Pmgr.onchkDefault);
			PMPlus.Common.globals.chkPmgrctxMenu.setAttribute("permission","pmplus-contextmenu");
			PMPlus.Common.globals.chkPmgrctxMenu.setAttribute("global-permission","dom.event.contextmenu.enabled");
			PMPlus.Common.globals.chkPmgrHistory.addEventListener("command",PMPlus.Pmgr.onchkDefault);
			PMPlus.Common.globals.chkPmgrHistory.setAttribute("permission","pmplus-history");
			PMPlus.Common.globals.chkPmgrHistory.setAttribute("global-permission","places.history.enabled");
			PMPlus.Pmgr.rbgClipboard.addEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.Pmgr.rbgClipboard.setAttribute("permission","pmplus-clipboard");
			PMPlus.Pmgr.rbgctxMenu.addEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.Pmgr.rbgctxMenu.setAttribute("permission","pmplus-contextmenu");
			PMPlus.Pmgr.rbgHistory.addEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.Pmgr.rbgHistory.setAttribute("permission","pmplus-history");
			PMPlus.Pmgr.prefListenerUA.register();
			PMPlus.Pmgr.prefListenerDOM.register();
			PMPlus.Pmgr.btnSave.setAttribute("disabled","true");
			PMPlus.Pmgr.rbgClipboard.selectedIndex=Services.prefs.getBoolPref("dom.event.clipboardevents.enabled",true) ? 0 : 1;
			PMPlus.Pmgr.rbgctxMenu.selectedIndex=Services.prefs.getBoolPref("dom.event.contextmenu.enabled",true) ? 0 : 1;
			PMPlus.Pmgr.rbgHistory.selectedIndex=Services.prefs.getBoolPref("places.history.enabled",true) ? 0 : 1;
			PMPlus.Pmgr.lstSites.addEventListener("select",PMPlus.Pmgr.onSiteSelectionChange);
			Services.obs.addObserver(PMPlus.Pmgr.permissionsObserver,"perm-changed",false);
			PMPlus.Pmgr.toggleSections(true);
		},
		/**
		 * Event handler for about:permissions list of sites.
		 */
		onSiteSelectionChange:function(event){
			if (event.target.selectedItem.id == "all-sites-item") {
				PMPlus.Pmgr.toggleSections(true);
			} else {
				PMPlus.Pmgr.docURI = Services.io.newURI(event.target.value,null,null);
				PMPlus.Pmgr.host = PMPlus.Pmgr.docURI.host.replace("www.","");
				PMPlus.Pmgr.rbgClipboard.setAttribute("pageurl",PMPlus.Pmgr.docURI.spec);
				PMPlus.Pmgr.rbgctxMenu.setAttribute("pageurl",PMPlus.Pmgr.docURI.spec);
				PMPlus.Pmgr.rbgHistory.setAttribute("pageurl",PMPlus.Pmgr.docURI.spec);
				PMPlus.Pmgr.toggleSections(false);
				PMPlus.Pmgr.populateSectionUA();
				var permclipboard = Services.perms.testPermission(PMPlus.Pmgr.docURI,"pmplus-clipboard");
				var permctxmenu = Services.perms.testPermission(PMPlus.Pmgr.docURI,"pmplus-contextmenu");
				var permhistory = Services.perms.testPermission(PMPlus.Pmgr.docURI,"pmplus-history");
				PMPlus.Common.initPermissions(permclipboard,
				PMPlus.Pmgr.rbgClipboard,PMPlus.Common.globals.chkPmgrClipboard,
				"dom.event.clipboardevents.enabled");
				PMPlus.Common.initPermissions(permctxmenu,
				PMPlus.Pmgr.rbgctxMenu,PMPlus.Common.globals.chkPmgrctxMenu,
				"dom.event.contextmenu.enabled");
				PMPlus.Common.initPermissions(permhistory,
				PMPlus.Pmgr.rbgHistory,PMPlus.Common.globals.chkPmgrHistory,
				"places.history.enabled");
			}
		},
		/**
		 * Populate the user-agent section.
		 */
		populateSectionUA:function(){
			PMPlus.Pmgr.txtUserAgent.value=PMPlus.Common.getOverride(PMPlus.Pmgr.host);
			if(PMPlus.Pmgr.txtUserAgent.value=="" || !PMPlus.Common.isOverridden(PMPlus.Pmgr.host)){
				PMPlus.Pmgr.txtUserAgent.placeholder=PMPlus.Common.getPlaceHolderText(PMPlus.Pmgr.host);
			}
		},
		/**
		 * Hide the extension's sections on the 'all sites' page.
		 */
		toggleSections:function(hidden){
			PMPlus.Pmgr.sectionUAHeader.setAttribute("hidden",hidden);
			PMPlus.Pmgr.sectionUAControls.setAttribute("hidden",hidden);
			PMPlus.Pmgr.sectionClipboardHeader.setAttribute("hidden",hidden);
			PMPlus.Common.hideSection(hidden,PMPlus.Pmgr.rbgClipboard,PMPlus.Pmgr.sectionClipboardControls);
			PMPlus.Pmgr.sectionCtxMenuHeader.setAttribute("hidden",hidden);
			PMPlus.Common.hideSection(hidden,PMPlus.Pmgr.rbgClipboard,PMPlus.Pmgr.sectionCtxMenuControls);
			PMPlus.Pmgr.sectionHistoryHeader.setAttribute("hidden",hidden);
			PMPlus.Common.hideSection(hidden,PMPlus.Pmgr.rbgHistory,PMPlus.Pmgr.sectionHistoryControls);
		},
		/**
		 * Event handler for save button.
		 */
		onBtnSave:function(){
			PMPlus.Common.prefBranchUA.setCharPref(PMPlus.Pmgr.host,PMPlus.Pmgr.txtUserAgent.value);
		},
		/**
		 * Event handler for reset button.
		 */
		onBtnReset:function(){
			PMPlus.Common.prefBranchUA.clearUserPref(PMPlus.Pmgr.host);
			PMPlus.Pmgr.txtUserAgent.placeholder=PMPlus.Common.prefBranchUA.getCharPref(PMPlus.Pmgr.host,"");
			PMPlus.Pmgr.txtUserAgent.value="";
			PMPlus.Pmgr.txtUserAgent.placeholder=PMPlus.Common.getPlaceHolderText(PMPlus.Pmgr.host);
			PMPlus.Pmgr.btnSave.setAttribute("disabled","true");
		},
		/**
		 * Event handler for UA textbox change.
		 */
		onUAChange:function(event){
			if(event.target.value.length==0||
				event.target.value==PMPlus.Common.getOverride(PMPlus.Pmgr.host)){
				PMPlus.Pmgr.btnSave.setAttribute("disabled","true");
			} else {
				PMPlus.Pmgr.btnSave.removeAttribute("disabled");
			}
		},
		/**
		 * Preference listener for user agent changes to current site.
		 */
		prefListenerUA:new PMPlus.PMPlusPrefListener("general.useragent.override.",
		    function(branch,name){
			if(name==PMPlus.Pmgr.host){
				PMPlus.Pmgr.txtUserAgent.value=PMPlus.Common.getOverride(PMPlus.Pmgr.host);
			}
		}),
		/**
		 * Eventhandler for clipboard permission checkbox.
		 */
		onchkDefault : function(event){
			switch(event.target){
				case PMPlus.Common.globals.chkPmgrClipboard:
					PMPlus.Common.useDefaultHandler(PMPlus.Pmgr.docURI,
					event.target,PMPlus.Pmgr.rbgClipboard,
					PMPlus.Common.globals.chkPgClipboard);
					break;
				case PMPlus.Common.globals.chkPmgrctxMenu:
					PMPlus.Common.useDefaultHandler(PMPlus.Pmgr.docURI,
					event.target,PMPlus.Pmgr.rbgctxMenu,
					PMPlus.Common.globals.chkPgctxMenu);
					break;
				case PMPlus.Common.globals.chkPmgrHistory:
					PMPlus.Common.useDefaultHandler(PMPlus.Pmgr.docURI,
					event.target,PMPlus.Pmgr.rbgHistory,
					PMPlus.Common.globals.chkPmgrHistory);
					break;
				default:break;
			}
		},
		/**
		 * Observes permission changes.
		 */
		permissionsObserver : {
			observe : function(aSubject,aTopic,aData){
				if(aTopic == "perm-changed") {
					if(aData == "deleted"){
						if(aSubject.type == "pmplus-clipboard"){
							PMPlus.Common.globals.chkPmgrClipboard.checked = true;
							PMPlus.Pmgr.rbgClipboard.disabled = true;
							PMPlus.Pmgr.rbgClipboard.selectedIndex = Services.prefs.getBoolPref("dom.event.clipboardevents.enabled",true) ? 0 : 1;
						} else if(aSubject.type == "pmplus-contextmenu"){
							PMPlus.Common.globals.chkPmgrctxMenu.checked = true;
							PMPlus.Pmgr.rbgctxMenu.disabled = true;
							PMPlus.Pmgr.rbgctxMenu.selectedIndex = Services.prefs.getBoolPref("dom.event.contextmenu.enabled",true) ? 0 : 1;
						} else if(aSubject.type == "pmplus-history"){
							PMPlus.Common.globals.chkPmgrHistory.checked = true;
							PMPlus.Pmgr.rbgHistory.disabled = true;
							PMPlus.Pmgr.rbgHistory.selectedIndex = Services.prefs.getBoolPref("places.history.enabled",true) ? 0 : 1;
						}
					} else if(aData == "added" || aData == "changed"){
						if(aSubject.type == "pmplus-clipboard"){
							PMPlus.Common.globals.chkPmgrClipboard.checked = false;
							PMPlus.Pmgr.rbgClipboard.selectedIndex = aSubject.capability - 1;
							PMPlus.Pmgr.rbgClipboard.disabled = false;
						} else if(aSubject.type == "pmplus-contextmenu"){
							PMPlus.Common.globals.chkPmgrctxMenu.checked = false;
							PMPlus.Pmgr.rbgctxMenu.selectedIndex = aSubject.capability - 1;
							PMPlus.Pmgr.rbgctxMenu.disabled = false;
						} else if(aSubject.type == "pmplus-history"){
							PMPlus.Common.globals.chkPmgrHistory.checked = false;
							PMPlus.Pmgr.rbgHistory.selectedIndex = aSubject.capability - 1;
							PMPlus.Pmgr.rbgHistory.disabled = false;
						}
					}
				}
			}
		},
		/**
		 * Preference listener for global DOM preference.
		 */
		prefListenerDOM:new PMPlus.PMPlusPrefListener("dom.event.",function(branch,name){
			var enabled = false;
			switch(name){
				case("clipboardevents.enabled"):
					enabled = branch.getBoolPref("clipboardevents.enabled",true);
					PMPlus.Common.hideSection(enabled,
					PMPlus.Pmgr.rbgClipboard,PMPlus.Pmgr.sectionClipboardControls);
					break;
				case("contextmenu.enabled"):
					enabled = branch.getBoolPref("clipboardevents.enabled",true);
					PMPlus.Common.hideSection(enabled,
					PMPlus.Pmgr.rbgctxMenu,PMPlus.Pmgr.sectionCtxMenuControls);
					break;
				default:
					break;
			}
		}),
		cleanup:function(){
			Services.obs.removeObserver(PMPlus.Pmgr.permissionsObserver,"perm-changed");
			PMPlus.Pmgr.rbgctxMenu.removeEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.Pmgr.rbgClipboard.removeEventListener("command",PMPlus.Common.onRbgPermission);
			PMPlus.Common.globals.chkPmgrctxMenu.removeEventListener("command",PMPlus.Pmgr.onchkDefault);
			PMPlus.Common.globals.chkPmgrClipboard.removeEventListener("command",PMPlus.Pmgr.onchkDefault);
			PMPlus.Pmgr.lstSites.removeEventListener("select",PMPlus.Pmgr.onSiteSelectionChange);
			PMPlus.Pmgr.btnSave.removeEventListener("command",PMPlus.Pmgr.onBtnSave);
			PMPlus.Pmgr.btnReset.removeEventListener("command",PMPlus.Pmgr.onBtnReset);
			PMPlus.Pmgr.txtUserAgent.removeEventListener("input",PMPlus.Pmgr.onUAChange);
			PMPlus.Pmgr.prefListenerDOM.unregister();
			PMPlus.Pmgr.prefListenerUA.unregister();
			PMPlus.Common.globals.chkPmgrClipboard=null;
			window.removeEventListener("load",PMPlus.Pmgr.init);
		}
}
window.addEventListener("load",PMPlus.Pmgr.init);
window.addEventListener("unload",PMPlus.Pmgr.cleanup);