if ("undefined" == typeof(PMPlus)) {
	var PMPlus = {};
}
Components.utils.import("chrome://permissionsplus/content/common.jsm",PMPlus);
PMPlus.Cu.import("resource://gre/modules/AddonManager.jsm");
PMPlus.Init={
		blockedEventsClipboard:["copy","cut","paste"],
		init:function(){
			var firstRun = PMPlus.Common.prefBranchExt.getBoolPref("firstrun",true);
			if(firstRun){
				PMPlus.Common.prefBranchExt.setBoolPref("firstrun",false);
				PMPlus.Common.prefBranchExt.setBoolPref("logging",false);
			} else {
				PMPlus.Common.setLogging(PMPlus.Common.prefBranchExt.getBoolPref("logging"));
			}
			PMPlus.Init.prefListener.register();
			gBrowser.addEventListener("load",PMPlus.Init.onPageLoad,true);
			AddonManager.addAddonListener(PMPlus.Common.uninstallListener);
			PMPlus.Common.historyService.addObserver(PMPlus.Init.historyObserver,false);
			Services.obs.addObserver(PMPlus.Common.uninstallObserver,"quit-application",false);
		},
		/**
		 * Called when any webpage loads. Actual blocking happens here.
		 */
		onPageLoad:function(event){
			let doc = event.originalTarget;
			if (doc instanceof HTMLDocument) {
				if (doc.defaultView.frameElement) {
					while (doc.defaultView.frameElement) {
						doc = doc.defaultView.frameElement.ownerDocument;
					}
				}
				try {
					if(gBrowser.currentURI.scheme.startsWith('http')){
						PMPlus.Common.printlog("On page "
						+gBrowser.currentURI.spec+" clipboard= "
						+Services.perms.testPermission(gBrowser.currentURI,"pmplus-clipboard")
						+" contextmenu = "+Services.perms.testPermission(gBrowser.currentURI,"pmplus-contextmenu"));
						//function injected into page to block particular events.
						var block = function(event){
								event.stopPropagation();
								PMPlus.Common.printlog("Blocked "+event.type+" event on page "+gBrowser.currentURI.spec);				
							}
						if(Services.perms.testPermission(gBrowser.currentURI,"pmplus-clipboard")==Services.perms.DENY_ACTION){
							for(event of PMPlus.Init.blockedEventsClipboard){
								if(Services.els.hasListenersFor(doc,event)){
									doc.addEventListener(event,block,true);
								}
							}
						}
						if(Services.perms.testPermission(gBrowser.currentURI,"pmplus-contextmenu")==Services.perms.DENY_ACTION){
							if(Services.els.hasListenersFor(doc,"contextmenu")){
									doc.addEventListener("contextmenu",block,true);
							}
						}
					}
				} catch(e){
					PMPlus.Cu.reportError(e);
				}
			}		
		},
		/**
		 * History observer removes history for current host if set to block.
		 */
		historyObserver : {
			onVisit(aURI,aVisitID,aTime,aSessionID,aReferringID,aTransitionType,aGUID){
				if(Services.perms.testPermission(aURI,"pmplus-history") == Services.perms.DENY_ACTION){
					PMPlus.Common.printlog("Removing pages for URI "+aURI.spec);
					PMPlus.Common.browserHistory.removePagesFromHost(aURI.host,true);
				}
			}
		},
		prefListener : new PMPlus.PMPlusPrefListener("extensions.pmplus.",function(branch,name){
			switch(name){
				case "logging" : PMPlus.Common.logoutput = branch.getBoolPref("logging",false);
				break;
			}
		}),
		cleanup:function(){
			Services.obs.removeObserver(PMPlus.Common.uninstallObserver,"quit-application");
			PMPlus.Common.historyService.removeObserver(PMPlus.Init.historyObserver);
			AddonManager.removeAddonListener(PMPlus.Common.uninstallListener);
			gBrowser.removeEventListener("load",PMPlus.Init.onPageLoad);
			PMPlus.Init.prefListener.unregister();
			window.removeEventListener("load",PMPlus.Init.init);
		}
}
window.addEventListener("load",PMPlus.Init.init,false);
window.addEventListener("unload",PMPlus.Init.cleanup,false);