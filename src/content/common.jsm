var EXPORTED_SYMBOLS = ["Common","Ci","Cu","PMPlusPrefListener"]
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
if ("undefined" == typeof(PMPlus)) {
	var PMPlus = {};
}
Common={
		logoutput:false,
		globals:{
			cmdClipboard : null,
			chkPmgrClipboard : null,
			chkPgClipboard : null,
			chkPmgrctxMenu : null,
			chkPgctxMenu : null,
			chkPmgrHistory : null,
			chkPgHistory : null
		},
		ID : "{c3303c6e-5424-41a3-8e65-255687a94434}",
		browserHistory : Cc["@mozilla.org/browser/nav-history-service;1"].getService(Ci.nsIBrowserHistory),
		historyService :Cc["@mozilla.org/browser/nav-history-service;1"].getService(Ci.nsINavHistoryService),
		uninstall : false,
		strBundle : Services.strings.createBundle("chrome://permissionsplus/locale/permissionsplus.properties"),
		prefBranchUA : Services.prefs.getBranch('general.useragent.override.'),
		prefBranchExt : Services.prefs.getBranch('extensions.permissionsplus.'),
		setLogging : function(flag){
			Common.logoutput=flag;
		},
		/**
		 * Log output to console if enabled.
		 */ 
		printlog:function(item) {
			if(Common.logoutput==true) {
				Services.console.logStringMessage("[PermissionsPlus:]"+item);
			}
		},
		/**
		 * Initialize UI depending on existing permissions.
		 */
		initPermissions : function(perm,rbGroup,chkDefault,globalPerm){
			//0 = UNKNOWN_ACTION, 1= ALLOW_ACTION, 2 = DENY_ACTION
			var override = perm > Services.perms.UNKNOWN_ACTION; 
			if(override){
				rbGroup.selectedIndex = (perm - 1);
			} else {
				rbGroup.selectedIndex = Services.prefs.getBoolPref(globalPerm, true) ? 0 : 1;
				chkDefault.checked = !override;
				Common.toggleRadioButtons(override,rbGroup);
			}
		},
		/**
		 * Toggles enable/disable for permission radiobuttons. 
		 */
		toggleRadioButtons : function(enable,rbGroup){
			if(enable){
				rbGroup.removeAttribute("disabled");
				rbGroup.getItemAtIndex(0).removeAttribute("disabled");
				rbGroup.getItemAtIndex(1).removeAttribute("disabled");
			} else {
				rbGroup.setAttribute("disabled","true");
				rbGroup.getItemAtIndex(0).setAttribute("disabled","true");
				rbGroup.getItemAtIndex(1).setAttribute("disabled","true");
			}
		},
		/**
		 * Common eventhandler for Allow/Block radiobutton groups.
		 */
		onRbgPermission : function(event){
			var URI = Services.io.newURI(event.target.radioGroup.getAttribute("pageurl"),null,null);
			Services.perms.add(URI,event.target.radioGroup.getAttribute("permission"),
			event.target.radioGroup.selectedIndex==0 ? Services.perms.ALLOW_ACTION:Services.perms.DENY_ACTION);
		},
		/**
		 * Handler for 'use default' checkbox.
		 */
		useDefaultHandler:function(pageURI,chkBox,rbGroup,chkOther){
			if(!chkBox.checked){
				rbGroup.removeAttribute("disabled");
				Common.toggleRadioButtons(true,rbGroup);
				Services.perms.add(pageURI,chkBox.getAttribute("permission"),
				rbGroup.selectedIndex==0 ? Services.perms.ALLOW_ACTION:Services.perms.DENY_ACTION);
				if(chkOther != null){
					chkOther.checked = false;
				}
			} else {
				rbGroup.selectedIndex = Services.prefs.getBoolPref(chkBox.getAttribute("global-permission")) ? 0 : 1;
				rbGroup.setAttribute("disabled","true");
				Services.perms.remove(pageURI,chkBox.getAttribute("permission"));
				Common.toggleRadioButtons(false,rbGroup);
				if(chkOther != null){
					chkOther.checked = true;
				}
			}
		},
		/**
		 * Returns the UA override, or blank if none present.
		 */
		getOverride:function(host){
			var retval = Common.prefBranchUA.getCharPref(host,"");
			return retval;
		},
		/**
		 * Hide appropriate section and set radiobuttons 
		 * when corresponding global DOM preference changes. 
		 */
		hideSection : function(hidden,rbGroup,section){
			rbGroup.selectedIndex = hidden ? 0 : 1;
			section.setAttribute("hidden",hidden ? "true": "false");
		},
		getPlaceHolderText:function(host){
			var retval = Common.getOverride(host); 
			if(retval.length==0){
				return Common.strBundle.GetStringFromName("noUAPlaceHolder");
			} else if(!Common.isOverridden(host)){
				return retval;
			}
		},
		/**
		 * Return true if an existing default UA was overridden.
		 */
		isOverridden:function(host){
			return Common.prefBranchUA.prefHasUserValue(host);
		},
		/**
		 * Check whether extension is being uninstalled and undo if not. 
	 	*/
		uninstallListener :{
			onUninstalling:function(addon){
				if(addon.id==Common.ID){
					Common.uninstall=true;
				}
			},
			onOperationCancelled: function(addon) {
    			if (addon.id == Common.ID) {
      				Common.uninstall = (addon.pendingOperations & AddonManager.PENDING_UNINSTALL) != 0;
      			}
    		}	
		},
		/**
	 	* Clean up settings when browser is restarting after a confirmed uninstall.
	 	*/
		uninstallObserver :{
			observe : function(aSubject,aTopic,aData){
				if(aTopic == "quit-application" && Common.uninstall == true) {
					Services.prefs.deleteBranch('extensions.permissionsplus.');
				}
			}
		},
		/**
		 * Preference listener for this extension.
		 */
		prefListenerExt : new PMPlusPrefListener("extensions.permissionsplus.",function(branch,name){
			switch(name){
			case("logging"):
				Common.setLogging(Services.prefs.getBoolPref("extensions.permissionsplus.logging",false));
			break;
			default:break;	
			}
		})
}
function PMPlusPrefListener(branch_name, callback) {
	// Keeping a reference to the observed preference branch or it will get
	// garbage collected.
	this._branch = Services.prefs.getBranch(branch_name);
	this._branch.QueryInterface(Ci.nsIPrefBranch2);
	this._callback = callback;
}

PMPlusPrefListener.prototype.observe = function(subject, topic, data) {
	if (topic == 'nsPref:changed')
		this._callback(this._branch, data);
};

PMPlusPrefListener.prototype.register = function() {
	this._branch.addObserver('', this, false);
	let that = this;
	this._branch.getChildList('', {}).
	forEach(function (pref_leaf_name){ 
		that._callback(that._branch, pref_leaf_name); 
	});
};

PMPlusPrefListener.prototype.unregister = function() {
	if (this._branch)
		this._branch.removeObserver('', this);
};